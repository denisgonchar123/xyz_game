// Fill out your copyright notice in the Description page of Project Settings.


#include "MyFirstModuleActor.h"

// Called when the game starts or when spawned
void AMyFirstModuleActor::BeginPlay()
{
	Super::BeginPlay();
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Hello from MyFirstModule"));
}

