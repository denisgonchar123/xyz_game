// Fill out your copyright notice in the Description page of Project Settings.


#include "firstLessonGameInstance.h"

const FColor& UfirstLessonGameInstance::GetPlayerColor()
{
	while (PlayerColor == FColor::Black)
	{
		PlayerColor = FColor::MakeRandomColor();
	}
	return PlayerColor;


}
