// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class XYZ_Game : ModuleRules
{
	public XYZ_Game(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "PhysXVehicles" });
	}
}
