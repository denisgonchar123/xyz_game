// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyFirstModuleActor.h"
#include "MyFirstModuleActorChild.generated.h"

/**
 * 
 */
UCLASS()
class XYZ_GAME_API AMyFirstModuleActorChild : public AMyFirstModuleActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	
};
