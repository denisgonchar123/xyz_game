// Copyright Epic Games, Inc. All Rights Reserved.

#include "XYZ_GameGameMode.h"
#include "XYZ_GameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AXYZ_GameGameMode::AXYZ_GameGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
