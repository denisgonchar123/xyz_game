// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "firstLessonGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class XYZ_GAME_API UfirstLessonGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	const FColor& GetPlayerColor();


private:
	FColor PlayerColor = FColor::Black;

};
